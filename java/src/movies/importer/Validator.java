package movies.importer;
import java.util.ArrayList;

/**
 * @author Phan Hieu Nghia
 */
public class Validator extends Processor {

	public Validator(String sourceDirectory, String outputDirectory) {

		super(sourceDirectory, outputDirectory, false);
	}

	/**
	 * Validates each movie to ensure they have the proper format.
	 *
	 * @param input An ArrayList<String> representing the input from the normalized folder.
	 * @return Returns a validated ArrayList<String>
	 */
	public ArrayList<String> process(ArrayList<String> input) {
        
		ArrayList<String> validatedInput = new ArrayList<String>();
        for(int i = 0; i < input.size(); i++) {

            String[] splitLine = input.get(i).split("\\t");
            for(int j = 0; j < splitLine.length; j++) {

                if(splitLine[j] != "" || splitLine[j] != null) {

                    try {

                        if(j == 0) {

                            Integer.parseInt(splitLine[j]);
                        } else if(j == 2) {
                            
                            Integer.parseInt(splitLine[j]);
                            validatedInput.add(input.get(i));
                        }
                    } catch(Exception e) {
                        continue;
                    }
                }
            }
        }
        return validatedInput;
    }
}
