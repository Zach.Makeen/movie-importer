package movies.importer;

/**
 * @author Zacharie Makeen
 */
public class Movie {

	private String year;
	private String title;
	private String runtime;
	private String source;

	/**
	 * Sets the values of Movie's 4 fields.
	 *
	 * @param year, title, runtime, source
	 */
	public Movie(String year, String title, String runtime, String source) {

		this.year = year;
		this.title = title;
		this.runtime = runtime;
		this.source = source;
	}

	/**
	 * Allows classes using the Movie type to access the year variable.
	 *
	 * @return year
	 */
	public String getYear() {

		return this.year;
	}

	/**
	 * Allows classes using the Movie type to access the title variable.
	 *
	 * @return title
	 */
	public String getTitle() {

		return this.title;
	}

	/**
	 * Allows classes using the Movie type to access the runtime variable.
	 *
	 * @return runtime
	 */
	public String getRuntime() {

		return this.runtime;
	}

	 /**
	  * Allows classes using the Movie type to access the source variable.
	  *
	  * @return source
	  */
	 public String getSource() {

	 	return this.source;
	 }

	/**
	 * Returns the information about a movie
	 *
	 * @return String containing information about a movie.
	 */
	 public String toString() {

	 	return (this.year + "\t" + this.title + "\t" + this.runtime + "\t" + this.source);
	 }

	 /**
	 * Checks each element of a given movie.
	 *
	 * @param object
	 * @return true or false
	 */
	@Override
	public boolean equals(Object o) {

		if(!(o instanceof Movie)) {

			return false;
		} else {

			Movie movie = (Movie)o;
			if(this.year.equals(movie.year)) {

			 	if(this.title.equals(movie.title)) {

			 		if((Integer.parseInt(this.runtime) - Integer.parseInt(movie.runtime)) <= 5 || (Integer.parseInt(movie.runtime) - Integer.parseInt(this.runtime)) <= 5) {

			 			return true;
			 		}
			 	}
			}
		}
		return false;
	 }
}