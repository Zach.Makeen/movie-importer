package movies.importer;
import java.util.ArrayList;

/**
 * @author Zacharie Makeen
 */
public class Normalizer extends Processor {

	public Normalizer(String sourceDirectory, String outputDirectory) {

		super(sourceDirectory, outputDirectory, false);
	}

	/**
	 * Normalizes the combined files to have the same format altogether.
	 *
	 * @param input An ArrayList<String> representing the input from the processed folder.
	 * @return Returns a normalized ArrayList<String>
	 */
	public ArrayList<String> process(ArrayList<String> input) {

		ArrayList<String> normalizedInput = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {

			String[] splitLine = input.get(i).split("\t");
			String[] runtime = splitLine[2].split(" ");
			Movie details = new Movie(splitLine[0], splitLine[1].toLowerCase(), runtime[0], splitLine[3]);
			normalizedInput.add(details.toString());
		}
		return normalizedInput;
	}
}
