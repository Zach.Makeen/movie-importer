package movies.importer;
import java.util.ArrayList;

/**
 * @author Zacharie Makeen
 */
public class KaggleImporter extends Processor {

	public KaggleImporter(String sourceDirectory, String outputDirectory) {

		super(sourceDirectory, outputDirectory, true);
	}

	/**
	 * Formats the input to resemble the format from the Movie's toString()
	 *
	 * @param input An ArrayList<String> representing the kaggleSmallFile
	 * @return Returns a formatted ArrayList<String>
	 */
	public ArrayList<String> process(ArrayList<String> input) {

		ArrayList<String> formattedInput = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {

			String[] splitLine = input.get(i).split("\\t", -1);
			if(splitLine.length == 21) {

				Movie details = new Movie(splitLine[20], splitLine[15], splitLine[13], "kaggle");
				formattedInput.add(details.toString());
			}
		}
		return formattedInput;
	}
}