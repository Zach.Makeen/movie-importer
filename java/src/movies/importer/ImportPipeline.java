package movies.importer;
import java.io.IOException;

/**
 * @author Zacharie Makeen & Phan Hieu Nghia
 */
public class ImportPipeline {

	/**
	 * Assigns the sources to their proper places and calls the processAll method.
	 */
	public static void main(String[] args) throws IOException {

		String[] sources = {"C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\movie-text-files\\Kaggle",
							"C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\movie-text-files\\Imdb",
							"C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\processed",
							"C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\normalized",
							"C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\validated",
							"C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\deduped"};

		KaggleImporter kaggleFormat = new KaggleImporter(sources[0], sources[2]);
		ImdbImporter imdbFormat = new ImdbImporter(sources[1], sources[2]);
		Normalizer normalized = new Normalizer(sources[2], sources[3]);
		Validator validated = new Validator(sources[3], sources[4]);
		Deduper deduped = new Deduper(sources[4], sources[5]);

		Processor[] process = {kaggleFormat, imdbFormat, normalized, validated, deduped};
		processAll(process);
	}

	/**
	 * Calls execute() on each of the processors.
	 *
	 * @param input
	 */
	public static void processAll(Processor[] input) throws IOException {

		for(int i = 0; i < input.length; i++) {

			input[i].execute();
		}
	}
}
