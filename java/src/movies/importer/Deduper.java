package movies.importer;
import java.util.ArrayList;

/**
 * @author Zacharie Makeen & Phan Hieu Nghia
 */
public class Deduper extends Processor {

	public Deduper(String sourceDirectory, String outputDirectory) {

		super(sourceDirectory, outputDirectory, false);
	}

	/**
	 * Dedupes the file to remove any duplicate movies.
	 *
	 * @param input An ArrayList<String> representing the input from the validated folder.
	 * @return Returns a deduped ArrayList<String>
	 */
	public ArrayList<String> process(ArrayList<String> input) {

		ArrayList<Movie> convertedInput = new ArrayList<Movie>();
		ArrayList<String> dedupedInput = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {
			
			String[] splitLine = input.get(i).split("\t", -1);
			Movie movie = new Movie(splitLine[0], splitLine[1], splitLine[2], splitLine[3]);
			if(!(convertedInput.contains(movie))) {

				convertedInput.add(movie);
			} else {

				int index = convertedInput.indexOf(movie);
				String[] modification = convertedInput.get(index).toString().split("\t", -1);
				if(modification[3] != splitLine[3]) {

					modification[3] += ";" + splitLine[3];
					convertedInput.set(index, new Movie(splitLine[0], splitLine[1], splitLine[2], modification[3]));
				}
			}
		}
		for(int i = 0; i < convertedInput.size(); i++) {

			dedupedInput.add(convertedInput.get(i).toString());
		}
		return dedupedInput;
	}
}
