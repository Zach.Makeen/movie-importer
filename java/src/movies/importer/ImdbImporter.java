package movies.importer;
import java.util.ArrayList;

/**
 * @author Phan Hieu Nghia
 */
public class ImdbImporter extends Processor {

	public ImdbImporter(String sourceDirectory, String outputDirectory) {

		super(sourceDirectory, outputDirectory, true);
	}

	/**
	 * Formats the input to resemble the format from the Movie's toString()
	 *
	 * @param input An ArrayList<String> representing the imdbSmallFile
	 * @return Returns a formatted ArrayList<String>
	 */
	public ArrayList<String> process(ArrayList<String> input) {

		ArrayList<String> formattedInput = new ArrayList<String>();
		for(int i = 0; i < input.size(); i++) {
			
			String[] splitLine = input.get(i).split("\\t", -1);
			if(splitLine.length == 22) {
				
				Movie details = new Movie(splitLine[3], splitLine[1], splitLine[6], "imdb");
				formattedInput.add(details.toString());
			}
		}
		return formattedInput;
	}
}
