package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import movies.importer.ImdbImporter;

/**
 * @author Zacharie Makeen
 */
class NormalizerTests {

	private Normalizer normalized = new Normalizer("C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\processed", "C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\normalized");
						
	@Test
	void processTest() {

		ArrayList<String> test = new ArrayList<String>();
		test.add("1996\tSpy Hard\t80 minutes\tkaggle");
		String expected = "1996\tspy hard\t80\tkaggle";
		
		assertEquals(expected, normalized.process(test).get(0));
	}

}
