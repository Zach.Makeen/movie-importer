package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import movies.importer.ImdbImporter;

/**
 * @author Zacharie Makeen
 */
class ValidatorTests {

	private Validator validated = new Validator("C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\processed", "C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\normalized");
						
	@Test
	void processTest() {

		ArrayList<String> test = new ArrayList<String>();
		test.add("1996\tspy hard\t80\tkaggle");
		String expected = "1996\tspy hard\t80\tkaggle";
		
		assertEquals(expected, validated.process(test).get(0));
	}

}
