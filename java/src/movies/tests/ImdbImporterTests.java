package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import movies.importer.ImdbImporter;

/**
 * @author Phan Hieu Nghia
 */
class ImdbImporterTests {
	
	private ImdbImporter imdb = new ImdbImporter("C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\movie-text-files\\Imdb", "C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\processed");
						
	@Test
	void processTest() {

		ArrayList<String> test = new ArrayList<String>();
		test.add("tt0002101	Cleopatra	Cleopatra	1912	11/13/1912	\"Drama, History\"	100	USA	English	Charles L. Gaskill	Victorien Sardou	Helen Gardner Picture Players	\"Helen Gardner, Pearl Sindelar, Miss Fielding, Miss Robson, Helene Costello, Charles Sindelar, Mr. Howard, James R. Waite, Mr. Osborne, Harry Knowles, Mr. Paul, Mr. Brady, Mr. Corker\"	The fabled queen of Egypt's affair with Roman general Marc Antony is ultimately disastrous for both of them.	5.2	446	\"$45,000 \"				25	3");
		String expected = "1912\tCleopatra\t100\timdb";
		
		assertEquals(expected, imdb.process(test).get(0));
	}
}