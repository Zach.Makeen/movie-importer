package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import movies.importer.ImdbImporter;

/**
 * @author Zacharie Makeen & Phan Hieu Nghia
 */
class KaggleImporterTests {

	private KaggleImporter kaggle = new KaggleImporter("C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\movie-text-files\\Kaggle", "C:\\Users\\zacha\\OneDrive\\Semester III\\Java\\Projects\\processed");
						
	@Test
	void processTest() {

		ArrayList<String> test = new ArrayList<String>();
		test.add("Leslie Nielsen	Nicollette Sheridan	Andy Griffith	Marcia Gay Harden	John Ales	Barry Bostwick	\"Dead pan Leslie Nielsen stars as Dick Steele, agent WD-40, in this uneven send-up of spy thrillers and other popular genres done in the hilarious style of the Zucker/Abrams \"\"Naked Gun\"\" series. WD-40 is a James Bondish sort who gets called from retirement to stop his nefarious megalomaniacal and armless nemesis General Rancor from overtaking the world. Steele receives assistance from tasty Russian agent Veronique Ukrinsky. Movies massacred in the ensuing chaos include Speed, Pulp Fiction, True Lies, Sister Act, Home Alone, Jurrassic Park and even Butch Cassidy and the Sundance Kid. Several stars also make cameos including Ray Charles, Mr. T., Pat Morita, Robert Culp, Dr. Joyce Brothers and Fabio.\"	Rick Friedberg	Director Not Available	Director Not Available	Action	PG-13	5/24/1996	80 minutes	Hollywood Pictures	Spy Hard	Rick Friedberg	Dick Chudnow	Jason Friedberg	Aaron Seltzer	1996");
		String expected = "1996\tSpy Hard\t80 minutes\tkaggle";
		
		assertEquals(expected, kaggle.process(test).get(0));
	}

}
