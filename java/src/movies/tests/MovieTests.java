package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import java.util.ArrayList;
import org.junit.jupiter.api.Test;
import movies.importer.ImdbImporter;

/**
 * @author Phan Hieu Nghia & Zacharie Makeen
 */
class MovieTests {

	@Test
	void constructorTest() {

		String year = "1939";
		String title = "Gone with the wind";
		String runtime = "3h58";
		String source = "imdb";
		Movie test = new Movie(year, title, runtime, source);
		
		assertEquals(year, test.getYear());
		assertEquals(title, test.getTitle());
		assertEquals(runtime, test.getRuntime());
		assertEquals(source, test.getSource());
	}

	@Test
	void getYearTest() {

		Movie test = new Movie("1999", null, null, null);
		assertEquals("1999", test.getYear());
	}

	@Test
	void getTitleTest() {
		
		Movie test = new Movie(null, "Fast and Furious", null, null);
		assertEquals("Fast and Furious", test.getTitle());
	}

	@Test
	void getRuntimeTest() {

		Movie test = new Movie(null, null, "96", null);
		assertEquals("96", test.getRuntime());
	}

	@Test
	void getSourceTest() {

		Movie test = new Movie(null, null, null, "imdb");
		assertEquals("imdb",test.getSource());
	}

	@Test
	void toStringTest() {

		String year = "2006";
		String title = "The Fast and the Furious: Tokyo Drift";
		String runtime = "85";
		String source = "imdb";
		Movie test = new Movie(year, title, runtime, source);
		String toStringTest = (year + "\t" + title + "\t" + runtime + "\t" + source); 
		
		assertEquals(toStringTest, test.toString());
	}

}
